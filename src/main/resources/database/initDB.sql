CREATE TABLE IF NOT EXISTS developers (
  id         BIGINT PRIMARY KEY,
  first_name VARCHAR(50)  NOT NULL,
  last_name  VARCHAR(100) NOT NULL,
  salary     DECIMAL      NOT NULL
);