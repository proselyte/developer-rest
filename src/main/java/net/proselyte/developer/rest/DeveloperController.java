package net.proselyte.developer.rest;

import net.proselyte.developer.model.Developer;
import net.proselyte.developer.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "api/developers")
public class DeveloperController {

    @Autowired
    private DeveloperService developerService;

    @RequestMapping(value = "/{developerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Developer> getDeveloper(@PathVariable("developerId") Long developerId) {
        if (developerId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Developer developer = this.developerService.getById(developerId);

        if (developer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(developer, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Developer>> getDevelopers() {
        List<Developer> developers = this.developerService.getAll();

        if (developers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(developers, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Developer> saveDeveloper(@RequestBody @Valid Developer developer, BindingResult bindingResult, UriComponentsBuilder builder) {
        HttpHeaders headers = new HttpHeaders();

        System.out.println(developer);

        if (developer == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.developerService.save(developer);
        headers.setLocation(builder.path("/api/developers/{developerId}").buildAndExpand(developer.getId()).toUri());
        return new ResponseEntity<>(developer, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{developerId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> deleteDeveloper(@PathVariable("developerId") Long developerId) {
        Developer developer = this.developerService.getById(developerId);

        if (developer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.developerService.delete(developerId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
