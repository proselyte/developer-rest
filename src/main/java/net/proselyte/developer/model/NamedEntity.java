package net.proselyte.developer.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Base class that extends {@link BaseEntity} adding property NAME.
 *
 * @author Eugene Suleimanov
 */

@MappedSuperclass
@Getter
@Setter
@ToString
public class NamedEntity extends BaseEntity {

    @Column(name = "name")
    private String name;
}
