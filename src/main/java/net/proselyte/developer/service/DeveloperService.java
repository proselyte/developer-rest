package net.proselyte.developer.service;

import net.proselyte.developer.model.Developer;

import java.util.List;

public interface DeveloperService {
    Developer getById(Long id);

    void save(Developer developer);

    void delete(Long id);

    List<Developer> getAll();
}
