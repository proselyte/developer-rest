package net.proselyte.developer.service;

import lombok.extern.slf4j.Slf4j;
import net.proselyte.developer.model.Developer;
import net.proselyte.developer.repository.DeveloperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    DeveloperRepository developerRepository;

    @Override
    public Developer getById(Long id) {
        log.info("Getting developer by ID {}", id);
        return developerRepository.findOne(id);
    }

    @Override
    public void save(Developer developer) {
        log.info("Saving developer {}", developer);
        developerRepository.save(developer);
    }

    @Override
    public void delete(Long id) {
        log.info("Deleting developer with ID: {}", id);
        developerRepository.delete(id);
    }

    @Override
    public List<Developer> getAll() {
        log.info("Getting list of all developer");
        return developerRepository.findAll();
    }
}
